import React from "react";

const Home = () => (
  <React.Fragment>
    <p>Welcome to BudgetHub!</p>
  </React.Fragment>
);

export default Home;
