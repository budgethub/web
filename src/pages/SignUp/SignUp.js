import React from "react";
import PropTypes from "prop-types";

import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Avatar from "@material-ui/core/Avatar";
import LockIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";

class SignUp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      passwordConfirmation: "",
      firstName: "",
      lastName: "",
    };

    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handlePasswordConfirmationChange = this.handlePasswordConfirmationChange.bind(
      this,
    );
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleUsernameChange(event) {
    this.setState({ username: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  handlePasswordConfirmationChange(event) {
    this.setState({ passwordConfirmation: event.target.value });
  }

  handleFirstNameChange(event) {
    this.setState({ firstName: event.target.value });
  }

  handleLastNameChange(event) {
    this.setState({ lastName: event.target.value });
  }

  handleSubmit(event) {
    const {
      username,
      password,
      passwordConfirmation,
      firstName,
      lastName,
    } = this.state;
    const { dispatch, history } = this.props;
    const { signUp } = dispatch;

    signUp(
      history,
      username,
      password,
      passwordConfirmation,
      firstName,
      lastName,
    );

    event.preventDefault();
  }

  render() {
    const { classes } = this.props;
    const {
      username,
      password,
      passwordConfirmation,
      firstName,
      lastName,
    } = this.state;

    return (
      <React.Fragment>
        <CssBaseline />
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign Up
            </Typography>
            <form className={classes.form} onSubmit={this.handleSubmit}>
              <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="username">Username</InputLabel>
                <Input
                  autoComplete="username"
                  autoFocus
                  id="username"
                  name="username"
                  onChange={this.handleUsernameChange}
                  value={username}
                />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="password">Password</InputLabel>
                <Input
                  id="password"
                  name="password"
                  type="password"
                  value={password}
                  onChange={this.handlePasswordChange}
                />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="password-confirmation">
                  Confirm Password
                </InputLabel>
                <Input
                  id="password-confirmation"
                  name="password-confirmation"
                  type="password"
                  value={passwordConfirmation}
                  onChange={this.handlePasswordConfirmationChange}
                />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="first-name">First Name</InputLabel>
                <Input
                  autoComplete="first-name"
                  autoFocus
                  id="first-name"
                  name="first-name"
                  onChange={this.handleFirstNameChange}
                  value={firstName}
                />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="last-name">Last Name</InputLabel>
                <Input
                  autoComplete="last-name"
                  autoFocus
                  id="last-name"
                  name="last-name"
                  onChange={this.handleLastNameChange}
                  value={lastName}
                />
              </FormControl>
              <Button
                className={classes.submit}
                color="primary"
                fullWidth
                type="submit"
                variant="contained"
              >
                Sign Up
              </Button>
            </form>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

SignUp.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  dispatch: PropTypes.shape({
    signUp: PropTypes.func.isRequired,
  }).isRequired,
};

export default SignUp;
