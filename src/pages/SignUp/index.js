import React from "react";
import { withRouter } from "react-router-dom";
import withStyles from "@material-ui/core/styles/withStyles";

import SignUp from "./SignUp";
import styles from "./style";

import AuthenticationContext from "../../contexts/authentication";

const EnhancedSignUp = withRouter(withStyles(styles)(SignUp));

const SignUpWithContext = () => (
  <AuthenticationContext.Consumer>
    {({ state, dispatch }) => (
      <EnhancedSignUp authentication={state} dispatch={dispatch} />
    )}
  </AuthenticationContext.Consumer>
);

export default SignUpWithContext;
