import React from "react";
import { withRouter } from "react-router-dom";
import withStyles from "@material-ui/core/styles/withStyles";

import SignIn from "./SignIn";
import styles from "./style";

import AuthenticationContext from "../../contexts/authentication";

const EnhancedSignIn = withRouter(withStyles(styles)(SignIn));

const SignInWithContext = () => (
  <AuthenticationContext.Consumer>
    {({ state, dispatch }) => (
      <EnhancedSignIn authentication={state} dispatch={dispatch} />
    )}
  </AuthenticationContext.Consumer>
);

export default SignInWithContext;
