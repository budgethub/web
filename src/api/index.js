/* eslint import/prefer-default-export: 0 */

import callApi from "./base";

export const signIn = (username, password) =>
  callApi("token", "post", {}, { username, password });

export const signUp = (
  username,
  password,
  passwordConfirmation,
  firstName,
  lastName,
) =>
  callApi(
    "users",
    "post",
    {},
    {
      username,
      password,
      password_confirmation: passwordConfirmation,
      first_name: firstName,
      last_name: lastName,
    },
  );
