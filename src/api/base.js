/* eslint no-undef: 0 */

const API_HOST = process.env.REACT_APP_API_URL;

/* istanbul ignore next */
function getQueryString(params) {
  const esc = encodeURIComponent;
  return Object.keys(params)
    .map(k => `${esc(k)}=${esc(params[k])}`)
    .join("&");
}

async function callApi(endpoint, method = "get", headers = {}, body = {}) {
  let fullUrl =
    endpoint.indexOf(API_HOST) === -1 ? API_HOST + endpoint : endpoint;
  let requestBody;
  const requestHeaders = new Headers();
  requestHeaders.set("Content-Type", "application/json");

  /* istanbul ignore next */
  if (Object.keys(headers).length > 0) {
    Object.keys(headers).forEach(key => {
      if (Object.prototype.hasOwnProperty.call(headers, key)) {
        requestHeaders.set(key, headers[key]);
      }
    });
  }

  if (Object.keys(body).length > 0) {
    if (["get", "delete"].indexOf(method) > -1) {
      /* istanbul ignore next */
      fullUrl += `?${getQueryString(body)}`;
    } else {
      requestBody = JSON.stringify(body);
    }
  }

  const options = {
    method,
    headers: requestHeaders,
    body: requestBody,
  };

  try {
    const response = await fetch(fullUrl, options);
    const responseBody = await response.text();
    const responseJson = responseBody.length ? JSON.parse(responseBody) : {};
    const responseHeaders = response.headers;
    const responseStatus = response.status;

    return {
      body: responseJson,
      headers: responseHeaders,
      status: responseStatus,
    };
  } catch (error) {
    /* istanbul ignore next */
    throw error;
  }
}

export default callApi;
