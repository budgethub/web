import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import { BrowserRouter as Router, Route } from "react-router-dom";

import NavBar from "./components/NavBar";

import { AuthenticationProvider } from "./contexts/authentication";

import Home from "./pages/Home";
import SignIn from "./pages/SignIn";
import SignUp from "./pages/SignUp";

const App = () => (
  <Router>
    <AuthenticationProvider>
      <CssBaseline />
      <NavBar />
      <Route exact path="/" component={Home} />
      <Route path="/login" component={SignIn} />
      <Route path="/register" component={SignUp} />
    </AuthenticationProvider>
  </Router>
);

export default App;
