export const getAuthToken = () => {
  localStorage.getItem("authenticationToken");
};

export const setAuthToken = authToken => {
  localStorage.setItem("authenticationToken", authToken);
};

export const removeAuthToken = () => {
  localStorage.removeItem("authenticationToken");
};
