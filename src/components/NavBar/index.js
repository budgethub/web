import React from "react";
import { withStyles } from "@material-ui/core/styles";

import NavBar from "./NavBar";
import styles from "./style";

import AuthenticationContext from "../../contexts/authentication";

const NavBarWithStyle = withStyles(styles)(NavBar);

const NavBarWithContext = () => (
  <AuthenticationContext.Consumer>
    {({ state }) => <NavBarWithStyle authentication={state} />}
  </AuthenticationContext.Consumer>
);

export default NavBarWithContext;
