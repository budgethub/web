import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import AppBar from "@material-ui/core/AppBar";
import ToolBar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";

export const AuthenticatedMenu = () => (
  <Button color="inherit" component={Link} to="/logout">
    Sign Out
  </Button>
);

export const UnauthenticatedMenu = () => (
  <React.Fragment>
    <Button color="inherit" component={Link} to="/login">
      Sign In
    </Button>
    <Button color="inherit" component={Link} to="/register">
      Sign Up
    </Button>
  </React.Fragment>
);

export const UserControls = ({ isAuthenticated }) => {
  if (isAuthenticated) {
    return <AuthenticatedMenu />;
  }

  return <UnauthenticatedMenu />;
};

UserControls.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
};

const NavBar = ({ authentication: { isAuthenticated }, classes }) => (
  <div className={classes.root}>
    <AppBar position="static">
      <ToolBar>
        <IconButton
          className={classes.menuButton}
          color="inherit"
          aria-label="Menu"
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" color="inherit" className={classes.grow}>
          BudgetHub
        </Typography>
        <UserControls isAuthenticated={isAuthenticated} />
      </ToolBar>
    </AppBar>
  </div>
);

NavBar.propTypes = {
  authentication: PropTypes.shape({
    isAuthenticated: PropTypes.bool.isRequired,
  }).isRequired,
  classes: PropTypes.object.isRequired,
};

export default NavBar;
