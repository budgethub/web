import React from "react";
import { shallow } from "enzyme";

import NavBar, {
  UserControls,
  AuthenticatedMenu,
  UnauthenticatedMenu,
} from "../../components/NavBar/NavBar";

describe("NavBar", () => {
  it("renders without error in debug mode", () => {
    const authentication = {
      isAuthenticated: false,
    };
    const classes = {};
    const component = shallow(
      <NavBar debug authentication={authentication} classes={classes} />,
    );

    expect(component).toMatchSnapshot();
  });
});

describe("UserControls", () => {
  describe("when user is not authenticated", () => {
    it("displays the unauthenticated menu", () => {
      const component = shallow(<UserControls debug isAuthenticated={false} />);

      expect(component.find(UnauthenticatedMenu).length).toBe(1);
    });
  });

  describe("when user is authenticated", () => {
    it("displays the authenticated menu", () => {
      const component = shallow(<UserControls debug isAuthenticated />);

      expect(component.find(AuthenticatedMenu).length).toBe(1);
    });
  });
});

describe("AuthenticatedMenu", () => {
  it("renders without error in debug mode", () => {
    const component = shallow(<AuthenticatedMenu debug />);

    expect(component).toMatchSnapshot();
  });
});

describe("UnauthenticatedMenu", () => {
  it("renders without error in debug mode", () => {
    const component = shallow(<UnauthenticatedMenu debug />);

    expect(component).toMatchSnapshot();
  });
});
