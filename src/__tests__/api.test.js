import { signIn, signUp } from "../api";

describe("Authentication API", () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  describe("signIn", () => {
    it("returns the authentication token", async () => {
      const apiArgs = {
        username: "testuser",
        password: "password",
      };

      const returnObject = {
        id: 123,
        username: "testuser",
        first_name: "Test",
        last_name: "User",
      };

      const returnHeader = new Headers({ Authorization: "abcde12345" });

      const expectedResponse = {
        body: {
          id: 123,
          username: "testuser",
          first_name: "Test",
          last_name: "User",
        },
        headers: new Headers({
          Authorization: "abcde12345",
          "Content-Type": "text/plain;charset=UTF-8",
        }),
        status: 201,
      };

      fetch.mockResponseOnce(JSON.stringify(returnObject), {
        status: 201,
        headers: returnHeader,
      });

      const response = await signIn(apiArgs);

      expect(response).toEqual(expectedResponse);
      expect(fetch.mock.calls.length).toEqual(1);
    });
  });

  describe("signUp", () => {
    it("returns the authentication token", async () => {
      const apiArgs = {
        username: "testuser",
        password: "password",
        passwordConfirmation: "password",
        firstName: "Test",
        lastName: "User",
      };

      const returnObject = {
        id: 123,
        username: "testuser",
        first_name: "Test",
        last_name: "User",
      };

      const returnHeader = new Headers({ Authorization: "abcde12345" });

      const expectedResponse = {
        body: {
          id: 123,
          username: "testuser",
          first_name: "Test",
          last_name: "User",
        },
        headers: new Headers({
          Authorization: "abcde12345",
          "Content-Type": "text/plain;charset=UTF-8",
        }),
        status: 201,
      };

      fetch.mockResponseOnce(JSON.stringify(returnObject), {
        status: 201,
        headers: returnHeader,
      });

      const response = await signUp(apiArgs);

      expect(response).toEqual(expectedResponse);
      expect(fetch.mock.calls.length).toEqual(1);
    });
  });
});
