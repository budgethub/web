import React from "react";
import { shallow } from "enzyme";

import SignIn from "../../pages/SignIn/SignIn";

describe("SignIn", () => {
  const classes = {};
  const dispatch = {
    signIn: jest.fn(),
  };
  const history = {
    replace: jest.fn(),
  };

  it("renders without error in debug mode", () => {
    const component = shallow(
      <SignIn debug classes={classes} dispatch={dispatch} history={history} />,
    );

    expect(component).toMatchSnapshot();
  });

  it("calls handleUsernameChange when username field changes", () => {
    const spy = jest.spyOn(SignIn.prototype, "handleUsernameChange");
    const component = shallow(
      <SignIn debug classes={classes} dispatch={dispatch} history={history} />,
    );
    component
      .find("#username")
      .simulate("change", { target: { value: "testuser" } });
    expect(spy).toHaveBeenCalled();
  });

  it("calls handlePasswordChange when password field changes", () => {
    const spy = jest.spyOn(SignIn.prototype, "handlePasswordChange");
    const component = shallow(
      <SignIn debug classes={classes} dispatch={dispatch} history={history} />,
    );
    component
      .find("#password")
      .simulate("change", { target: { value: "secretpass" } });
    expect(spy).toHaveBeenCalled();
  });

  it("calls handleSubmit when form is submitted", () => {
    const spy = jest.spyOn(SignIn.prototype, "handleSubmit");
    const component = shallow(
      <SignIn debug classes={classes} dispatch={dispatch} history={history} />,
    );
    component.find("form").simulate("submit", { preventDefault: () => {} });
    expect(spy).toHaveBeenCalled();
  });
});
