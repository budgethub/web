import React from "react";
import { shallow } from "enzyme";

import SignUp from "../../pages/SignUp/SignUp";

describe("SignUp", () => {
  const classes = {};
  const dispatch = {
    signUp: jest.fn(),
  };
  const history = {
    replace: jest.fn(),
  };

  it("renders without error in debug mode", () => {
    const component = shallow(
      <SignUp debug classes={classes} dispatch={dispatch} history={history} />,
    );

    expect(component).toMatchSnapshot();
  });

  it("calls handleUsernameChange when username field changes", () => {
    const spy = jest.spyOn(SignUp.prototype, "handleUsernameChange");
    const component = shallow(
      <SignUp debug classes={classes} dispatch={dispatch} history={history} />,
    );
    component
      .find("#username")
      .simulate("change", { target: { value: "testuser" } });
    expect(spy).toHaveBeenCalled();
  });

  it("calls handlePasswordChange when password field changes", () => {
    const spy = jest.spyOn(SignUp.prototype, "handlePasswordChange");
    const component = shallow(
      <SignUp debug classes={classes} dispatch={dispatch} history={history} />,
    );
    component
      .find("#password")
      .simulate("change", { target: { value: "secretpass" } });
    expect(spy).toHaveBeenCalled();
  });

  it("calls handlePasswordConfirmationChange when password field changes", () => {
    const spy = jest.spyOn(
      SignUp.prototype,
      "handlePasswordConfirmationChange",
    );
    const component = shallow(
      <SignUp debug classes={classes} dispatch={dispatch} history={history} />,
    );
    component
      .find("#password-confirmation")
      .simulate("change", { target: { value: "secretpass" } });
    expect(spy).toHaveBeenCalled();
  });

  it("calls handleFirstNameChange when first name field changes", () => {
    const spy = jest.spyOn(SignUp.prototype, "handleFirstNameChange");
    const component = shallow(
      <SignUp debug classes={classes} dispatch={dispatch} history={history} />,
    );
    component
      .find("#first-name")
      .simulate("change", { target: { value: "Test" } });
    expect(spy).toHaveBeenCalled();
  });

  it("calls handleLastNameChange when last name field changes", () => {
    const spy = jest.spyOn(SignUp.prototype, "handleLastNameChange");
    const component = shallow(
      <SignUp debug classes={classes} dispatch={dispatch} history={history} />,
    );
    component
      .find("#last-name")
      .simulate("change", { target: { value: "User" } });
    expect(spy).toHaveBeenCalled();
  });

  it("calls handleSubmit when form is submitted", () => {
    const spy = jest.spyOn(SignUp.prototype, "handleSubmit");
    const component = shallow(
      <SignUp debug classes={classes} dispatch={dispatch} history={history} />,
    );
    component.find("form").simulate("submit", { preventDefault: () => {} });
    expect(spy).toHaveBeenCalled();
  });
});
