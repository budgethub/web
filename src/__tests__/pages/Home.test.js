import React from "react";
import { shallow } from "enzyme";

import Home from "../../pages/Home/Home";

describe("Home", () => {
  it("renders without error in debug mode", () => {
    const component = shallow(<Home debug />);

    expect(component).toMatchSnapshot();
  });
});
