/* eslint react/no-unused-state: 0 */

import React from "react";
import PropTypes from "prop-types";

import * as api from "../api";
import { setAuthToken } from "../utils/storage";

const AuthenticationContext = React.createContext();

export class AuthenticationProvider extends React.Component {
  constructor() {
    super();

    this.state = {
      authenticationToken: "",
      isAuthenticated: false,
    };

    this.signIn = this.signIn.bind(this);
    this.signUp = this.signUp.bind(this);
  }

  signIn(history, username, password) {
    api.signIn(username, password).then(response => {
      if (response.status === 201) {
        const authHeader = response.headers.get("Authorization");
        const authToken = authHeader.replace("Bearer ", "");

        setAuthToken(authToken);
        this.setState({
          authenticationToken: authToken,
          isAuthenticated: true,
        });
        history.replace("/");
      }
    });
  }

  signUp(
    history,
    username,
    password,
    passwordConfirmation,
    firstName,
    lastName,
  ) {
    api
      .signUp(username, password, passwordConfirmation, firstName, lastName)
      .then(response => {
        if (response.status === 201) {
          const authHeader = response.headers.get("Authorization");
          const authToken = authHeader.replace("Bearer ", "");

          setAuthToken(authToken);
          this.setState({
            authenticationToken: authToken,
            isAuthenticated: true,
          });
          history.replace("/");
        }
      });
  }

  render() {
    const { children } = this.props;

    return (
      <AuthenticationContext.Provider
        value={{
          state: this.state,
          dispatch: { signIn: this.signIn, signUp: this.signUp },
        }}
      >
        {children}
      </AuthenticationContext.Provider>
    );
  }
}

AuthenticationProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AuthenticationContext;
